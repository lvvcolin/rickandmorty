﻿using System.Net.Http.Headers;

namespace WebApplication1.rickandmortyapi
{
    public class ApiCall
    {

        public HttpClient HttpClient { get; set; }



        public ApiCall()
        {
            this.HttpClient = new HttpClient();
            //base adress from the site i get the api from
            HttpClient.BaseAddress = new Uri("https://rickandmortyapi.com");
            //send headers to the browser
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public  HttpResponseMessage GetApiresult(string url) 
        {
            return  this.HttpClient.GetAsync(url).Result;    
            
        }

        
    }
}
