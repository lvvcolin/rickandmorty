﻿namespace WebApplication1.Models.ViewModels
{
    public class CharacterLocationEpisodeViewModel
    {
        public Episode episode {get;set;}
        public location location {get;set; }
        public Character Characters {get;set; }
    }
}
