﻿namespace WebApplication1.Models
{
    public class Result
    {
        public int id { get; set; }
        public string name { get; set; }
        public string species { get; set; }
        public string gender { get; set; }
        public string status { get; set; }
        public object origin { get; set; }
    }
}
