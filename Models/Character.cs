﻿using System.Reflection.Metadata;

namespace WebApplication1.Models
{
    public class Character
    {
        public page info { get; set; }
        public List<CharacterResult> results { get; set; }
    }

    public class page
    {
        public int count { get; set; }
        public int pages { get; set; }
        public string next { get; set; }
        public string prev { get; set; }

    }

    public class CharacterResult
    {
            public int id { get; set; }
            public string name { get; set; }
            public string species { get; set; }
            public string gender { get; set; }
            public string status { get; set; }
            public object origin { get; set; }
            public string image { get; set; }
            public CharacterLocation location { get; set; }
    }
    public class CharacterLocation
    {
        public string name { get; set; }

        public string url { get; set; }
    }
}
