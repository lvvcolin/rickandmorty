﻿namespace WebApplication1.Models
{
    public class location
    {
        public List<locationResult> results { get; set; }
    }
    public class locationResult
    {
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string dimension { get; set; }
        public string url { get; set; }
        public List<object> residents { get; set; }
    }

}
