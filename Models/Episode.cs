﻿namespace WebApplication1.Models
{
    public class Episode
    {
        public object? info { get; set; }
        public List<EpisodeResult> results { get; set; }
    }

    public class EpisodeResult
    {
        public int id { get; set; }
        public string name { get; set; }
        public string air_date { get; set; }
        public string episode { get; set; }
        public string status { get; set; }
        public List<object> characters { get; set; }

    }
}
