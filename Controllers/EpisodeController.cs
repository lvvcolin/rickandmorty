﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WebApplication1.Models;
using WebApplication1.Models.ViewModels;
using WebApplication1.rickandmortyapi;

namespace WebApplication1.Controllers
{
    public class EpisodeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            ApiCall apiCall = new ApiCall();

            HttpResponseMessage apiResultCharacter = apiCall.GetApiresult("/api/episode");
            var messageLocation = await apiResultCharacter.Content.ReadAsStringAsync();

            Episode location = JsonSerializer.Deserialize<Episode>(messageLocation);


            return View(location);
        }

        public async Task<IActionResult> Show(int id)
        {
            var apiCall = new ApiCall();

            HttpResponseMessage apiResultEpisode = apiCall.GetApiresult(String.Format("api/episode/{0}",id));
            var episodeResponse = await apiResultEpisode.Content.ReadAsStringAsync();
            EpisodeResult episodeResult = JsonSerializer.Deserialize<EpisodeResult>(episodeResponse);


            var CharacterResultList = new List<CharacterResult>();

            foreach(var character in episodeResult.characters)
            {
                var apiCharacter = apiCall.GetApiresult(character.ToString());
                var characterResponse = apiCharacter.Content.ReadAsStream();

                CharacterResult CharacterResult = JsonSerializer.Deserialize<CharacterResult>(characterResponse);

                CharacterResultList.Add(CharacterResult);
            }


            episodeCharacterViewModel episodeCharacterViewModel = new episodeCharacterViewModel
            {
                CharacterResult = CharacterResultList,
                EpisodeResult = episodeResult,
            };

            return View(episodeCharacterViewModel);
        }


    }
}
