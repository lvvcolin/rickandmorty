﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WebApplication1.Models;
using WebApplication1.Models.ViewModels;
using WebApplication1.rickandmortyapi;

namespace WebApplication1.Controllers
{
    public class LocationController : Controller
    {
        public async Task<IActionResult> Index()
        {
            ApiCall apiCall = new ApiCall();

            HttpResponseMessage apiResultCharacter = apiCall.GetApiresult("/api/location");
            var messageLocation = await apiResultCharacter.Content.ReadAsStringAsync();

            location location = JsonSerializer.Deserialize<location>(messageLocation);


            return View(location);
        }
        public async Task<IActionResult> Show(string url)
        {
            var apiCall = new ApiCall();

            HttpResponseMessage apiResultEpisode = apiCall.GetApiresult(url);
            var characterLocationMessages = await apiResultEpisode.Content.ReadAsStringAsync();
            locationResult characterLocation = JsonSerializer.Deserialize<locationResult>(characterLocationMessages);

            List<CharacterResult> CharacterResult = new List<CharacterResult>();

            foreach (var character in characterLocation.residents)
            {
                HttpResponseMessage apiResultCharacter = apiCall.GetApiresult(character.ToString());
                var characterMessages = await apiResultCharacter.Content.ReadAsStringAsync();
                var response = JsonSerializer.Deserialize<CharacterResult>(characterMessages);
                CharacterResult.Add(response);
            }

            CharacterLocationViewModel characterLocationViewModel = new CharacterLocationViewModel
            {
                characterresult = CharacterResult,
                locationResult = characterLocation,
            };

            return View(characterLocationViewModel);
        }

    }
}
