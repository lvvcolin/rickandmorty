﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApplication1.Models;
using WebApplication1.Models.ViewModels;
using System.Text.Json; 
using System.Text.Json.Serialization;

namespace WebApplication1.Controllers
{


    using System;
    using System.Collections;
    using System.Net.Http.Headers;
    using WebApplication1.rickandmortyapi;

    public class HomeController : Controller
    {        
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;

        }
        public async  Task<ActionResult> Index()
        {
        ApiCall apiCall = new ApiCall();

        HttpResponseMessage apiResultCharacter = apiCall.GetApiresult("/api/character");
        var messageCharacter  = await apiResultCharacter.Content.ReadAsStringAsync();

        HttpResponseMessage apiResultLocation = apiCall.GetApiresult("/api/location");
        var messagelocation = await apiResultLocation.Content.ReadAsStringAsync();

        HttpResponseMessage apiResultEpisode = apiCall.GetApiresult("/api/episode");
        var messageEpisode = await apiResultEpisode.Content.ReadAsStringAsync();



        Character characters = JsonSerializer.Deserialize<Character>(messageCharacter);
        location location = JsonSerializer.Deserialize<location>(messagelocation);
        Episode episode = JsonSerializer.Deserialize<Episode>(messageEpisode);



            var CharacterLocationEpisodeViewModel = new CharacterLocationEpisodeViewModel()
            {
                Characters = characters,
                location = location,
                episode = episode
            };



            return View(CharacterLocationEpisodeViewModel);
           

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}