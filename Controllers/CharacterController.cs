﻿using Gobln.Pager;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WebApplication1.Models;
using WebApplication1.rickandmortyapi;

namespace WebApplication1.Controllers
{
    public class CharacterController : Controller
    {
        public async Task< IActionResult> Index(int page = 1)
        {


            ViewBag.page = page;

            ApiCall apicall = new ApiCall();
            var call = apicall.GetApiresult(string.Format("api/character?page={0}", page) );
            string result = await call.Content.ReadAsStringAsync();

            Character response = JsonSerializer.Deserialize<Character>(result);

            ViewBag.pages = response.info.pages;

            return View(response);
        }
        public async Task<IActionResult> show(int id)
        {
            ApiCall apicall = new ApiCall();
            var call = apicall.GetApiresult(String.Format("api/character/{0}",id) );
            string result = await call.Content.ReadAsStringAsync();


            CharacterResult response = JsonSerializer.Deserialize<CharacterResult>(result);

            return View(response);
        }

    }
}
